package br.senai.sp.informatica.cadastro.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.senai.sp.informatica.cadastro.model.Aluno;

public interface AlunoRepo extends JpaRepository<Aluno, Long> {
    Page<Aluno> findByNomeContainingIgnoreCase(String nome, Pageable paging);
}
