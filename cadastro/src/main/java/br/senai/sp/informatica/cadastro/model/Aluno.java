package br.senai.sp.informatica.cadastro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Aluno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(min = 5, max = 150, message = "Nome com mínimo 5 e máximo 150 caracteres")
    @Column(unique = true)
    private String nome;
    @Email(message = "E-mail inválido")
    @Column(unique = true)
    private String email;
    @Min(value = 16, message = "Idade mínima é 16 anos")
    @Max(value = 90, message = "Idade máxima é 90 anos")
    private int idade;    
}
