package br.senai.sp.informatica.cadastro.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.informatica.cadastro.component.DataException;
import br.senai.sp.informatica.cadastro.component.DataNotFoundException;
import br.senai.sp.informatica.cadastro.component.DataValidationException;
import br.senai.sp.informatica.cadastro.model.Aluno;
import br.senai.sp.informatica.cadastro.service.AlunoService;


@RestController
@RequestMapping("/api")
public class AlunoController {
    @Autowired
    private AlunoService service;

    @PostMapping("/aluno")
    public ResponseEntity<Object> salvar(
        @RequestBody @Valid Aluno obj, BindingResult result) throws DataException {
            if(result.hasErrors()) {
                throw new DataValidationException("Aluno com dados inválidos", result);
            } else {
                Aluno aluno;
                if(obj.getId() == null) {
                    aluno = service.criar(obj);
                } else {
                    aluno = service.salvar(obj);
                }
                return ResponseEntity.ok(aluno);
            }
    }

    @RequestMapping(value="/alunos", method=RequestMethod.GET, produces = "application/json;charset=utf-8")
    public ResponseEntity<Page<Aluno>> listar(
            @RequestParam(defaultValue = "") String nome,
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "nome") String sortBy) {
        return ResponseEntity.ok(service.listar(nome, pageNo, pageSize, sortBy));
    }
    
    // public ResponseEntity<List<Aluno>> listar() {
    //     return ResponseEntity.ok(service.listar());
    // }
    
    @GetMapping("/aluno/{id}")
    public ResponseEntity<Aluno> localizar(@PathVariable(value = "id") long id) throws DataException {
        Optional<Aluno> aluno = service.localizar(id);

        if(aluno.isPresent()) {
            return ResponseEntity.ok(aluno.get());
        } else {
            throw new DataNotFoundException("Aluno não encontrado");
        }
    }
    
    @DeleteMapping("/aluno/{id}")
    public ResponseEntity<Object> remover(@PathVariable(value = "id") long id) throws DataException {
        service.remover(id);
        return ResponseEntity.ok().build();
    }
}
