package br.senai.sp.informatica.cadastro.component;

public class DataNotFoundException extends DataException {
    public DataNotFoundException(String message) {
        super(message);
    }

    public DataNotFoundException(String message, Throwable t) {
        super(message, t);
    }
}
