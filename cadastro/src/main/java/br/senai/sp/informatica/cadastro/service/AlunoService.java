package br.senai.sp.informatica.cadastro.service;

import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.cadastro.component.DataException;
import br.senai.sp.informatica.cadastro.component.DataNotFoundException;
import br.senai.sp.informatica.cadastro.model.Aluno;
import br.senai.sp.informatica.cadastro.repo.AlunoRepo;

@Service
public class AlunoService {
    @Autowired
    private AlunoRepo repo;
    
 
    public Page<Aluno> listar(String nome, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
    
        if(nome.isEmpty()) {
            return repo.findByNomeContainingIgnoreCase(nome, paging);
        } else {
            return repo.findAll(paging);
        }
    }
    
    // public List<Aluno> listar() {
    //     return repo.findAll();
    // }

    public Aluno criar(Aluno aluno) throws DataException {
        try {
            return repo.save(aluno);
        } catch(DataIntegrityViolationException ex) {
            throw new DataNotFoundException("Nome do Aluno duplicado");
        }
    }

    public Aluno salvar(Aluno obj) throws DataException {
        try {
            Aluno aluno = repo.getById(obj.getId());
            aluno.setNome(obj.getNome());
            aluno.setEmail(obj.getEmail());
            aluno.setIdade(obj.getIdade());
            return repo.save(aluno);
        } catch(JpaObjectRetrievalFailureException | EntityNotFoundException ex) {
            throw new DataNotFoundException("Aluno não encontrado");
        }
    }

    public void remover(long id) throws DataException {
        try {
            repo.delete(localizar(id).get());
        } catch(NoSuchElementException ex) {
            throw new DataNotFoundException("Aluno não encontrado");
        }
    }

    public Optional<Aluno> localizar(long id) throws DataException {
        try {
            return repo.findById(id);
        } catch(JpaObjectRetrievalFailureException ex) {
            throw new DataNotFoundException("Aluno não encontrado");
        }
    }
}
